<?php

include_once '../config/init.php';;

use App\User\User;
use App\User\Auth;
use App\Message\Message;
if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    //var_dump($image_name);
    $temporary_location= $_FILES['image']['tmp_name'];

    move_uploaded_file( $temporary_location,'../../resources/images/'.$image_name);
    $_POST['image']=$image_name;

}
//var_dump($_POST);
$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isExist();

if($status) {
    Message::message('User already exists using this email', 'danger');
    header('Location: ../index.php');
}else {

    $user->prepare($_POST)->store();
}

