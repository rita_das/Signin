<?php
include_once '../config/init.php';

use App\Home\Home;
use App\User\Auth;
use App\Message\Message;

$home = new Home();

$auth = new Auth();

$resourcePath = '../resources/login-signup-modal-window/';

$status = $auth->prepare($_POST)->isLoggedIn();

$sHome = $home->prepare($_GET)->singleHome();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Home | <?= $sHome['title'] ?></title>
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/jquery-ui/jquery-ui.css">
</head>
<body>

<header>

    <nav class="navbar navbar-light bg-faded">
        <div class="container">
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2" aria-controls="exCollapsingNavbar2" aria-expanded="false" aria-label="Toggle navigation">
                &#9776;
            </button>
            <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
                <a class="navbar-brand" href="../index.php">Reservation</a>
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

</header>

<div class="hero-image">

</div>

<div class="home-title-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-block">
                    <h3 class="card-title"><?= $sHome['title'] ?></h3>
                    <p class="card-text"><b>Location :</b> <?= $sHome['location'] ?></p>
                    <div class="row">
                        <div class="col-sm-4">
                            <p><b>Bedrooms :</b> <?= $sHome['bedrooms'] ?></p>
                        </div>
                        <div class="col-sm-4">
                            <p><b>Bathrooms :</b> <?= $sHome['bathrooms'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="booking-box">
                    <div class="card">
                        <form action="book.php" method="post">
                            <div class="card-header">
                                <p>Price <?= $sHome['price'] ?>$ Per Night</p>
                            </div>
                            <div class="card-block">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="from" class="card-title">Check In</label>
                                        <p class="form-group">
                                            <input type="text" id="from" name="from" class="form-control">
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="to" class="card-title">Check Out</label>
                                        <p class="form-group">
                                            <input type="text" id="to" name="to" class="form-control">
                                        </p>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary" id="bookNow">Book Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../bower_components/tether/dist/js/tether.min.js"></script>
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../resources/jquery-ui/jquery-ui.min.js"></script>

<script>
    $( function() {
        var array = ["2016-08-17","2016-08-03","2013-03-16"];

        var dateFormat = "yy-mm-dd",
            from = $( "#from" )
                .datepicker({
                    defaultDate: "+1D",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: "yy-mm-dd",
                    beforeShowDay: function(date){
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [ array.indexOf(string) == -1 ]
                    }
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                    console.log(getDate( this ));
                }),
            to = $( "#to" ).datepicker({
                    defaultDate: "+2D",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: "yy-mm-dd"
                })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                    console.log(getDate( this ));
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );

//  Book Now Event
//    $('#bookNow').click(function(evt){

//        evt.preventDefault();

//        var date1 = from.datepicker("getDate");
//        console.log(date1);
//    });
</script>

</body>
</html>
