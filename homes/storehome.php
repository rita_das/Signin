<?php
include_once '../config/init.php';

use App\Home\Home;
use App\User\Auth;
use App\Message\Message;

$home = new Home();
$auth = new Auth();
//
//var_dump($_POST);
//die();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: ../index.php');
}

$home->prepare($_POST)->store();