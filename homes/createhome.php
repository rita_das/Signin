<?php
include_once '../config/init.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$resourcePath = '../../resources/responsive-sidebar-navigation/';

$user = new User();;
$auth = new Auth();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: index.php');
}

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

$_POST['email'] = $_SESSION['user_email'];

$sUser  = $user->prepare($_POST)->view();

?>


<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?= $resourcePath ?>css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $resourcePath ?>css/style.css"> <!-- Resource style -->

    <script src="<?= $resourcePath ?>js/modernizr.js"></script> <!-- Modernizr -->

    <title>Dashboard | Reservation</title>
</head>
<body>
<header class="cd-main-header">
    <a href="#0" class="cd-logo"><img src="<?= $resourcePath ?>img/cd-logo.svg" alt="Logo"></a>

    <a href="#0" class="cd-nav-trigger">Menu<span></span></a>

    <nav class="cd-nav">
        <ul class="cd-top-nav">
            <li><a href="#0">Tour</a></li>
            <li><a href="#0">Support</a></li>
            <li class="has-children account">
                <a href="#0">
                    <img src="<?= $resourcePath ?>img/cd-avatar.png" alt="avatar">
                    Account
                </a>

                <ul>

                    <li><a href="#0">My Account</a></li>
                    <li><a href="#0">Edit Account</a></li>
                    <li><a href="../auth/logout.php">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header> <!-- .cd-main-header -->

<main class="cd-main-content">
    <nav class="cd-side-nav">
        <ul>
            <li class="cd-label">Main</li>
            <li class="has-children overview">
                <a href="#0">Overview</a>

                <ul>
                    <li><a href="#0">All Data</a></li>
                    <li><a href="#0">Category 1</a></li>
                    <li><a href="#0">Category 2</a></li>
                </ul>
            </li>
            <li class="has-children notifications active">
                <a href="#0">Notifications<span class="count">3</span></a>

                <ul>
                    <li><a href="#0">All Notifications</a></li>
                    <li><a href="#0">Friends</a></li>
                    <li><a href="#0">Other</a></li>
                </ul>
            </li>

            <li class="has-children comments">
                <a href="#0">Comments</a>

                <ul>
                    <li><a href="#0">All Comments</a></li>
                    <li><a href="#0">Edit Comment</a></li>
                    <li><a href="#0">Delete Comment</a></li>
                </ul>
            </li>
        </ul>

        <ul>
            <li class="cd-label">Secondary</li>
            <li class="has-children bookmarks">
                <a href="#0">Bookmarks</a>

                <ul>
                    <li><a href="#0">All Bookmarks</a></li>
                    <li><a href="#0">Edit Bookmark</a></li>
                    <li><a href="#0">Import Bookmark</a></li>
                </ul>
            </li>
            <li class="has-children images">
                <a href="#0">Create Home</a>
            </li>

            <li class="has-children users">
                <a href="#0">Users</a>

                <ul>
                    <li><a href="#0">All Users</a></li>
                    <li><a href="#0">Edit User</a></li>
                    <li><a href="#0">Add User</a></li>
                </ul>
            </li>
        </ul>

        <ul>
            <li class="cd-label">Action</li>
            <li class="action-btn"><a href="#0">+ Button</a></li>
        </ul>
    </nav>

    <div class="content-wrapper">
        <h1>Responsive Sidebar Navigation</h1>
        <h3>Welcome <?= $sUser['first_name'] ?></h3>

        <div class="row">
            <div class="col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Enter Your Home details
                    </div>
                    <div class="panel-body">
                        <!--sign up-->
                        <form action="storehome.php" method="post">
                            <input type="hidden" name="user_id" value="<?= $sUser['id'] ?>">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="location">Location</label>
                                <input type="text" name="location" id="location" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="bedrooms">Bedrooms</label>
                                <input type="number" name="bedrooms" id="bedrooms" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="bathrooms">Bathrooms</label>
                                <input type="number" name="bathrooms" id="bathrooms" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="price">Price Per Night</label>
                                <input type="number" name="price" id="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Create" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div> <!-- .content-wrapper -->
</main> <!-- .cd-main-content -->
<script src="<?= $resourcePath ?>js/jquery-2.1.4.js"></script>
<script src="<?= $resourcePath ?>js/jquery.menu-aim.js"></script>
<script src="<?= $resourcePath ?>js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>